<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\ArticlesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() 
{
    return view('welcome');
});

Route::get('/contact', function()
{
    return view('contact');
});

Route::get('/about', function()
{
    return view('about', [
        'articles' => App\Models\Article::latest()->get(),
    ]);
});

Route::get('/articles', [ArticlesController::class, 'index'])->name('articles.index');
Route::post('/articles', [ArticlesController::class, 'store'])->name('articles.store');
Route::get('/articles/create', [ArticlesController::class, 'create'])->name('articles.create');
Route::get('/articles/{article}', [ArticlesController::class, 'show'])->name('articles.show');
Route::get('/articles/{article}/edit', [ArticlesController::class, 'edit'])->name('articles.edit');
Route::put('/articles/{article}', [ArticlesController::class, 'update'])->name('articles.update');



// HANDLING ROUTE IN CLOSURE:
// Route::get('test/{test}', function ($test) {
//     $posts = [
//         'my_first_post' => 'Hello, this is my first blog post!',
//         'my_second_post' => 'Starting to get it...',
//     ];

//     if(!array_key_exists($test, $posts)) {
//         abort(404, 'Sorry, that post was not found');
//     }

//     return view('test', [
//         'post' => $posts[$test],
//     ]);
// });


// HANDLING ROUTE USING CONTROLLER
Route::get('/posts/{post}', [PostsController::class, 'show']);


/*
| ROUTE SYNTAX: when using parameters, enclose them with {} within the URI, and define them as parameters
| in the closure. When defining parameters, the name does not matter; they will be mapped to the order of
| the URI.
|
| When using the view function to render the view, enter the view's name as the first argument. After that,
| define an associative array. The keys of the AA will be available as variable names in the view, holding
| the value assigned in the view function.
|
| For bigger projects it's common to provide a controller instead of a closure as the second argument of
| the request function. In that case, provide the controller and handler function as the second argument
| using the following syntax:

|   [<ControllerName>::class, <method>]
|
| Also, make sure to import the controller file at the top of web.php!
*/